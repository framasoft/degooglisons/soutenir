/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function buildConfig(env) {
  const config = Object.assign({}, require('./common')(env)); // eslint-disable-line

  config.output.filename = '[name].bundle.js';

  Object.assign(config, {
    devServer: {
      contentBase: env.dist,
      historyApiFallback: true,
      hot: true,
      inline: true,
      open: false,
      publicPath: '/',
      writeToDisk: true,
    },
    devtool: 'eval-source-map',
    mode: 'development',
  });

  config.plugins.push(
    new HtmlWebpackPlugin({
      title: 'DEVELOPMENT prerender-spa-plugin',
      template: 'public/index.html',
      filename: 'index.html',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  );

  return config;
}

module.exports = buildConfig;
